<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Platos extends Model
{
    use HasFactory;

    protected $fillable = ['tab_color', 'tab_precio', 
        'tab_nombre', 'tab_inicio'];

}
