<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Models\Category;
use App\Models\Marca;
use App\Models\Medida;
use App\Models\Product;
use App\Models\Provider;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('product_index'), 403);

        $products = Product::all();

        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('product_create'), 403);

        $categories = Category::get();
        $providers = Provider::get();
        $medidas = Medida::get();
        $marcas = Marca::get();
        $warehouses = Warehouse::all();

        return view('admin.product.create', compact('categories', 'providers','marcas','medidas','warehouses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cantidad = 0;
        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $image_name = time() . '_' . $file->getClientOriginalName();
            $file->move(public_path("/image"), $image_name);
        }
        $product = Product::create($request->all()+[
            'image'=>$image_name,
        ]);

        if($product){
            $ids = $request->warehouses_id;
            foreach($ids as $id){
                DB::table('product_warehouses')->insert([
                    'product_id'=>$product->id,
                    'warehouse_id'=>$id,
                    'quantity'=>$_POST['warehouse_'.$id]
                ]);
                $cantidad+=$_POST['warehouse_'.$id];
            }

            $uproduct = Product::findOrFail($product->id);
            $uproduct->stock = $cantidad;
            $uproduct->update();
            $cantidad = 0;
        }
        
        if ($request->code == "") {
            $numero = $product->id;
            $numeroConCeros = str_pad($numero, 8, "0", STR_PAD_LEFT);

            $product->update([
                'code' => $numeroConCeros
            ]);
        }
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        abort_if(Gate::denies('product_show'), 403);

        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        abort_if(Gate::denies('product_edit'), 403);

        $categories = Category::all();
        $providers = Provider::all();
        $medidas = Medida::all();
        $marcas = Marca::all();
        $warehouses = Warehouse::all();
        $warehouses_selected = DB::table("product_warehouses")->select('warehouse_id','quantity')->where('product_id',$product->id)->get();

        return view('admin.product.edit', compact('product', 'categories', 'providers','marcas','medidas','warehouses','warehouses_selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());
        $cantidad = 0;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $image_name = time() . '_' . $file->getClientOriginalName();
            $file->move(public_path("/image"), $image_name);

            $product->update([
                'image' => $image_name
            ]);
        }

        if($product){
            DB::table('product_warehouses')->where('product_id',$product->id)->delete();
            $ids = $request->warehouses_id;
            foreach($ids as $id){
                DB::table('product_warehouses')->insert([
                    'product_id'=>$product->id,
                    'warehouse_id'=>$id,
                    'quantity'=>$_POST['warehouse_'.$id]
                ]);
                $cantidad+=$_POST['warehouse_'.$id];
            }

            $uproduct = Product::findOrFail($product->id);
            $uproduct->stock = $cantidad;
            $uproduct->update();
            $cantidad = 0;
        }

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        abort_if(Gate::denies('product_destroy'), 403);

        $product->delete();

        return redirect()->route('products.index');
    }

    public function change_status(Product $product)
    {
        if ($product->status == 'ACTIVE') {
            $product->update(['status' => 'DEACTIVATED']);
            return redirect()->back();
        } else {
            $product->update(['status' => 'ACTIVE']);
        }
        return redirect()->back();
    }


    public function get_products_by_barcode(Request $request)
    {
        if($request->ajax()){
            $products = Product::where('code',$request->code)->firstOrFail();
            return response()->json($products);
        }
    }

    public function get_products_by_id(Request $request)
    {
        if($request->ajax()){
            $products = Product::findOrFail($request->product_id);
            return response()->json($products);
        }
    }

    public function pdf(Product $product)
    {
        $compras = DB::table('purchase_details')
            ->where('purchase_details.product_id', $product->id)
            ->orderBy('purchase_details.created_at')
            ->select('quantity', 'price', 'created_at', DB::raw("'INGRESO' AS tipo"));
        $ventas = DB::table('sale_details')
            ->where('sale_details.product_id', $product->id)
            ->orderBy('sale_details.created_at')
            ->select('quantity', 'price', 'created_at', DB::raw("'SALIDA' AS tipo"));
        $existencias = $compras->union($ventas)->orderBy('created_at')->get();

        // dd($existencias);
        $subtotal = 0;
        foreach ($existencias as $key => $existencia) {
            if ($existencia->tipo == 'INGRESO') {
                $subtotal = $subtotal + $existencia->quantity;
                $existencias[$key]->saldo = $subtotal;
            } else {
                $subtotal = $subtotal - $existencia->quantity;
                $existencias[$key]->saldo = $subtotal;
            }
        }
        
        $pdf = \PDF::loadView('admin.product.pdf', compact('product', 'existencias', 'subtotal'));
        $pdf->set_paper('letter', 'landscape');
        return $pdf->stream('ReporteDeExistencia_'.$product->id.'.pdf');
    }
}
