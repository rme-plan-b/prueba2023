<?php

namespace App\Http\Controllers;

use App\Http\Requests\Platos\StoreRequest;
use App\Http\Requests\Platos\UpdateRequest;
use App\Models\Platos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PlatosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // abort_if(Gate::denies('category_index'), 403);
        $platos = Platos::all();

        return view('admin.platos.index', compact('platos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //abort_if(Gate::denies('category_create'), 403);

        return view('admin.platos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $platos = new Platos();
        if (str_word_count($request->tab_nombre, 0) > 1) {
            $platos->tab_nombre = $request->tab_nombre;
            $platos->tab_color = $request->tab_color;
            $platos->tab_precio = $request->tab_precio;
            $platos->tab_inicio = $request->tab_inicio;
            $platos->save();
        }
        return redirect()->route('platos.index');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Platos  $platos
     * @return \Illuminate\Http\Response
     */
    public function show(Platos $plato)
    {
        //abort_if(Gate::denies('category_show'), 403);

        return view('admin.platos.show', compact('plato'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Platos  $platos
     * @return \Illuminate\Http\Response
     */
    public function edit(Platos $plato)
    {
        //abort_if(Gate::denies('category_edit'), 403);

        return view('admin.platos.edit', compact('plato'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Platos  $plato
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Platos $plato)
    {
        $plato->update($request->all());

        return redirect()->route('platos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Platos  $plato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Platos $plato)
    {
        //abort_if(Gate::denies('category_destroy'), 403);

        $plato->delete();

        return redirect()->route('platos.index');
    }
}
