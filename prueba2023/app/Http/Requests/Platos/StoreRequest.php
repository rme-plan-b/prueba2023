<?php

namespace App\Http\Requests\Platos;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tab_nombre' => 'required|string|max:70',
            'tab_color' => 'required|string|max:70',
            'tab_precio' => 'required|int|max:70',
            'tab_inicio' => 'required|date|max:70',
        ];
    }

    public function messages()
    {
        return [
            'tab_nombre.required' => 'El nombre es requerido..',
            'tab_nombre.string' => 'El valor no es correcto..',
            'tab_nombre.max' => 'Solo se permite maximo 70 caracteres..',
        ];
    }
       
}
