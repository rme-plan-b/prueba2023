create table platos (
    tab_id int auto_increment primary key not null,
    tab_color text,
    tab_precio text,
    tab_nombre text,
    tab_inicio date,
    created_at timestamp,
    updated_at timestamp
);