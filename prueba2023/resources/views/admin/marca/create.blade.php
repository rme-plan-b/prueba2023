@extends('layouts.admin')

@section('title', 'Registrar Marca')
    
@section('options')

@endsection

@section('content')
    <div class="content-wrapper">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Panel Administrador</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('marcas.index') }}">Marca</a></li>
                    <li class="breadcrumb-item active">Registrar Marca</li>
                </ol>
            </nav>
        </div>

        <div class="container ">
            <div class="abs-center">
                <div class="card card-primary">
                    <div class="card-header" style="background-color:rgb(57, 62, 66); color:white">
                        <h1 style="text-align:center">REGISTRAR NUEVA MARCA</h1>
                    </div>
                    <div class="card-body">

                        {!! Form::open(['route' => 'marcas.store', 'method' => 'POST']) !!}

                            @include('admin.marca._form')
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-success mr-2">+ Registrar</button>
                                <a href="{{ route('marcas.index') }}" class="btn btn-info">Cancelar</a>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    
@endsection