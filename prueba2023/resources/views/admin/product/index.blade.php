@extends('layouts.admin')

@section('title', 'Gestión de Articulos')
    
@section('create')
{{-- <li class="nav-item d-none d-lg-flex">
    <a class="nav-link" href="{{ route('products.create') }}">
        <span class="btn btn-primary">+ Crear Producto</span>
    </a>
</li> --}}
@endsection

@section('styles')
    <style type="text/css">
        .unstyled-button {
            border: none;
            padding: 0;
            background: none
        }
    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">    
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Gestión de Artículos
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Panel Administrador</a></li>
                    <li class="breadcrumb-item active">Artículos</li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">

                        <div class="d-flex justify-content-between mb-4">
                            <h4>Lista de Artículos</h4>
                            <div>
                                    <a href="{{ route('products.create') }}" class="btn btn-success">+ Agregar Artículo</a>
                            </div>
                        </div>

                        <div>
                            <table id="detalle" class="table">
                                <thead class="bg-dark text-white">
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Código</th>
                                        <th>Stock</th>
                                        <th>Estado</th>
                                        <th>Imagen</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <th scope="row">{{ $product->id }}</th>
                                            <td>
                                                <a href="{{ route('products.show', $product) }}">{{ $product->name }}</a>
                                            </td>
                                            <td>{{ $product->codigo }}</td>
                                            <td>{{ $product->stock }}</td>
                                            <td>
                                                @if ($product->status == 'ACTIVE' )
                                                    <a href="{{ route('products.change.status', $product) }}" class="jsgrid-button btn btn-sm btn-success" title="Activo">
                                                            Activo <i class="fas fa-check"></i>
                                                    </a>
                                                @else
                                                    <a href="{{ route('products.change.status', $product) }}" class="jsgrid-button btn btn-sm btn-danger" title="Desactivado">
                                                            Desactivado <i class="fas fa-times"></i>
                                                    </a>
                                                @endif
                                            </td>
                                            <td>
                                                <img src="{{asset('image/'.$product->image )}}"  class="img-lg rounded-circle mb-3">
                                            </td>
                                            <td>
                                                {!! Form::open(['route' => ['products.destroy',$product], 'method' => 'DELETE']) !!}

                                                <a href="{{ route('products.pdf', $product) }}" title="Editar" class="btn btn-warning" style="padding: 6px 6px 6px 6px">
                                                        <i class="fas fa-clipboard-list" style="color: black"></i>
                                                    </a>

                                                    <a href="{{ route('products.edit', $product) }}" title="Editar" class="btn btn-warning" style="padding: 6px 6px 6px 6px">
                                                        <i class="far fa-edit" style="color: black"></i>
                                                    </a>

                                                    <button type="submit" title="Eliminar" class="btn btn-danger" style="padding: 6px 6px 6px 6px">
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>

                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {

            $('#detalle').DataTable({

                "order": [
                    [0, 'asc']
                ],
                "lengthMenu": [
                    [5, 10, 50, -1],
                    [5, 10, 50, "Todo"]
                ],
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "Ningun registro encontrado",
                    "info": "Mostrando la página _PAGE_ de _PAGES_",
                    "infoEmpty": "",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    'search': 'Buscar:',
                    'paginate': {
                        'next': 'Siguiente',
                        'previous': 'Anterior'
                    }

                },
            });

        });
    </script>
@endsection