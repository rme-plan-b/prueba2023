@extends('layouts.admin')

@section('title', 'Registrar Artículo')
    
@section('options')
    {{-- <li class="nav-item nav-settings d-none d-lg-block">
        <a href="#" class="nav-link">
            <i class="fa fa-elipsis-h"></i>
        </a>
    </li> --}}
@endsection

@section('content')
    <div class="content-wrapper">
        <div >

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Panel Administrador</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Lista de Artículos</a></li>
                    <li class="breadcrumb-item active">Registrar Artículo</li>
                </ol>
            </nav>
        </div>

        <div class="row">
            <div class="row justify-content-center">
                <div class="col-lg-8 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-header" style="background-color:rgb(57, 62, 66); color:white">
                            <h1 style="text-align:center">REGISTRAR NUEVO ARTÍCULO</h1>
                        </div>
                        <div class="card-body">
                            
                            {!! Form::open(['route' => 'products.store', 'method' => 'POST', 'files' => true]) !!} 
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="name">Nombre : </label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="" required>
                                </div>

                                <div class="form-group col-6">
                                    <label for="codigo">Código : </label>
                                    <input type="text" name="codigo" id="codigo" class="form-control" placeholder="" required>
                                </div>

                                <div class="form-group col-6">
                                  <label for="code">Código de Barras :</label>
                                  <input type="text" name="code" id="code" class="form-control" aria-describedby="helpId">
                                  <small id="helpId" class="text-muted">(Campo opcional)</small>
                                </div>
                                
                                <div class="form-group col-6">
                                    <label for="price">Precio de Venta : </label>
                                    <input type="number" name="price" id="price" class="form-control" placeholder="" required>
                                </div>

                                <div class="form-group col-6">
                                    <label for="stock">Stock Actual : </label>
                                    <input type="number" name="stock" id="stock" class="form-control" placeholder="" required>
                                </div>
                                
                                <div class="form-group col-6">
                                    <label for="provider_id">Proveedor : </label>
                                    <select class="form-control" name="provider_id" id="provider_id">
                                        <option> == Seleccione un Proveedor == </option>
                                        @foreach ($providers as $provider)
                                        <option value="{{ $provider->id }}">{{ $provider->name }}</option>                                      
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-6">
                                    <label for="category_id">Categoria : </label>
                                    <select class="form-control" name="category_id" id="category_id">
                                        <option> == Seleccione una Categoria == </option>
                                        @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>                                      
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-6">
                                    <label for="medida_id">Medida : </label>
                                    <select class="form-control" name="medida_id" id="medida_id">
                                        <option> == Seleccione una Medida == </option>
                                        @foreach ($medidas as $medida)
                                        <option value="{{ $medida->id }}">{{ $medida->nombre }}</option>                                      
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-6">
                                    <label for="marca_id">Marca : </label>
                                    <select class="form-control" name="marca_id" id="marca_id">
                                        <option> == Seleccione una Marca == </option>
                                        @foreach ($marcas as $marca)
                                        <option value="{{ $marca->id }}">{{ $marca->nombre }}</option>                                      
                                        @endforeach
                                    </select>
                                </div>
                                
                                {{-- <div class="form-group col-6">
                                    <label for="image">Imagen : </label>
                                    <div class="custom-file mb-4">
                                        <input type="file" class="custom-file-input" id="picture" name="picture" lang="es">
                                        <label for="image" class="custom-file-label">Seleccionar Archivo</label>
                                    </div>
                                </div> --}}

                                <div class="card-body">
                                    <h4 class="card-title d-flex">Imagen :
                                        <small class="ml-auto align-self-end">
                                            <a href="#" class="font-weight-light"
                                            >Seleccionar Archivo
                                            </a>
                                        </small>
                                    </h4>
                                    <input class="dropify" type="file" id="picture" name="picture">
                                </div>

                                    
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3>Lista de Almacenes</h3>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <th>Selecciona</th>
                                                    <th>Nombre</th>
                                                    <th>Cantidad</th>
                                                </thead>
                                                <tbody>
                                                    @foreach($warehouses as $w)
                                                        <tr>
                                                            <td><input type="checkbox" class="warehouse_class" name="warehouses_id[]" value="{{$w->id}}"></td>
                                                            <td>{{ $w->name }}</td>
                                                            <td><input type="text" required id="warehouses_id_{{$w->id}}" disabled name="warehouse_{{$w->id}}" class="form-control"></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>    
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-success mr-2">+ Registrar</button>
                                <a href="{{ route('products.index') }}" class="btn btn-info">Cancelar</a>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>
            </div>
        </div>
        </div>

    </div>
@endsection

@section('scripts')
    {!! Html::script('melody/js/dropify.js') !!}
    <script>
        $(document).ready(function(){
            $(".warehouse_class").click(function(){
                let id = $(this).val();
                if($(this).is(":checked")){
                    $("#warehouses_id_"+id).attr("disabled",false);
                }else{
                    $("#warehouses_id_"+id).val("").attr("disabled",true);
                }
            });
        });
    </script>
@endsection