<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proforma</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        @page {
            margin: 0cm 0cm;
            font-family: Arial;
        }
        body {
            margin: 3cm 2cm 2cm;
        }
        th, td { 
            padding-top: 2px !important;
            padding-bottom: 2px !important;
        }
    </style>
</head>
<body style="font-size:10px;">
    <div class="container mt-4">
        <div class="row">
            <div style="text-align: center;">
                <h6>IMPORTADORA DE REPUESTOS Y ACCESORIOS PARA<br>
                CAMION VOLVO</h6>
                Av. CHAPARE No 4647 Urbaniación La Huerta 16 Zona Quintanilla Sacaba<br>
                Telf.: 4724542 Cel.: 70761715-7979904 E-MAil: planeta-camion@hotmail.com
            </div>
            <div style="text-align: center">
                <h4>PROFORMA No. Guia {{ str_pad($proforma->id, 4, "0", STR_PAD_LEFT); }}</h4>
            </div>
            <div class="col-md-12">
                <table class="table" border="1">
                <tr>
                        <td width="20%">Cochabamba</td>
                        <td>{{ date("d") }}</td>
                        <td>de</td>
                        <td>{{ date("m") }}</td>
                        <td>de</td>
                        <td>{{ date("Y") }}</td>
                    </tr>
                    <tr>
                        <td>Señor(es)</td>
                        <td colspan="3"> {{ $proforma->client->name }} </td>
                        <td>Destino</td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table" border="1">
                    <thead>
                      <tr>
                        <th scope="col" width="2%">No.</th>
                        <th scope="col" width="15%">CÓDIGO</th>
                        <th scope="col" width="5%">CANT.</th>
                        <th scope="col">CONCEPTO</th>
                        <th scope="col" width="12%">P/U</th>
                        <th scope="col" width="13%">SUBTOTAL</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($saleDetails as $index => $item)
                        <tr style="">
                            <th scope="row" align="right">{{ $index + 1 }}</th>
                            <td>{{ $item->code }}</td>
                            <td align="right">{{ $item->quantity }}</td>
                            <td>{{ $item->name }}</td>
                            <td align="right">{{ $item->price }}</td>
                            <td align="right">{{ $item->subtotal }}</td>
                        </tr>
                    @endforeach
                    <!--/tbody>
                </table>
                <table class="table" border="1">
                    <tbody-->
                        <tr>
                            <td scope="row" colspan="4">Depositar a Cuenta de Zoila Diaz - BNB No de Cta 3500778015 - BANCO SOL  No Cta 1761383000001</td>
                            <td>TOTAL Bs</td>
                            <td align="right">{{ $subtotal }}</td>
                        </tr>
                        <tr>
                            <td scope="row" colspan="2">No. de cajas</td>
                            <td></td>
                            <td>Responsable: {{ Auth::user()->name }}</td>
                            <td>TOTAL US$</td>
                            <td align="right"></td>
                        </tr>
                        <tr>
                            <td scope="row" colspan="2">Fecha Vencimiento</td>
                            <td colspan="2"></td>
                            <td>T/CAMBIO</td>
                            <td align="right"></td>
                        </tr>
                        <tr><td scope="row" colspan="6"><br><br><br><br></td></tr>
                        <tr><td scope="row" colspan="2" align="right">FIRMA<br><br></td><td colspan="4"></td></tr>
                        <tr><td scope="row" colspan="2" align="right">RECIBI CONFORME<br><br></td><td colspan="4"></td></tr>
                        <tr><td scope="row" colspan="2" align="right">CI<br></td><td colspan="4"></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>