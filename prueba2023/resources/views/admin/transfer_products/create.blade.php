@extends('layouts.admin')

@section('title', 'Registrar Transferencia')
    
@section('options')
    {{-- <li class="nav-item nav-settings d-none d-lg-block">
        <a href="#" class="nav-link">
            <i class="fa fa-elipsis-h"></i>
        </a>
    </li> --}}
@endsection

@section('content')
    <div class="content-wrapper">
        <div >
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Panel Administrador</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('transfers.index') }}">Transferencias</a></li>
                    <li class="breadcrumb-item active">Registrar Transferencia</li>
                </ol>
            </nav>
        </div>
        <div class="container ">
            <div class="abs-center">
                <div class="card card-primary">
                    <div class="card-header" style="background-color:rgb(57, 62, 66); color:white">
                        <h1 style="text-align:center">REGISTRAR NUEVA TRANSFERENCIA DE INVENTARIO</h1>
                    </div>
                    <div class="card-body">

                        {!! Form::open(['route' => 'transfers.store', 'method' => 'POST', 'id'=>'send_create']) !!}

                            @include('admin.transfer_products._form')

                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-success mr-2">+ Registrar</button>
                                <a href="{{ route('transfers.index') }}" class="btn btn-info">Cancelar</a>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $("#warehouse_from_id").change(function(){
                let data = JSON.parse($("#warehouse_from_id option:selected").attr("data-products"));
                console.log(data);
                $("#product_id").empty();
                $("#product_id").append("<option value=''>Selecciona</option>");
                $.each(data, function(v,e){
                    $("#product_id").append("<option data-quantity='"+e.pivot.quantity+"' value='"+e.id+"'>"+e.name+" ("+e.pivot.quantity+")</option>");
                });
            });

            $("body").on('change','#product_id', function(){
                let quantity = $("#product_id option:selected").attr("data-quantity");
                $("#current_quantity").val(quantity);
            });

            $("#send_create").submit(function(){
                let quantity_selected = parseInt($("#current_quantity").val());
                let quantity = parseInt($("#quantity").val());
                if(quantity > quantity_selected){
                    alert("No puedes transferir un monto mayor a la existencia");
                    return false;
                }else{
                    return true;
                }
            });
        });
    </script
@endsection