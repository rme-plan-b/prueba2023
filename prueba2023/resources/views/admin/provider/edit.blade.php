@extends('layouts.admin')

@section('title', 'Editar Proveedor')
    
@section('options')
    {{-- <li class="nav-item nav-settings d-none d-lg-block">
        <a href="#" class="nav-link">
            <i class="fa fa-elipsis-h"></i>
        </a>
    </li> --}}
@endsection

@section('content')
    <div class="content-wrapper">
        <div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Panel Administrador</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Proveedor</a></li>
                    <li class="breadcrumb-item active">Editar Proveedor</li>
                </ol>
            </nav>
        </div>

        <div class="container ">
            <div class="abs-center">
                <div class="card card-primary">
                    <div class="card-header" style="background-color:rgb(57, 62, 66); color:white">
                        <h1 style="text-align:center">EDITAR PROVEEDOR</h1>
                    </div>
                    <div class="card-body">

                        {!! Form::model($provider, ['route' => ['providers.update', $provider], 'method' => 'PUT']) !!}
                    <div class="row">
                        <div class="form-group col-6">
                                <label for="name">Nombre :</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="" value="{{ $provider->name }}" required>
                            </div>
                            
                            {{-- <div class="form-group col-6">
                                <label for="email">Correo :</label>
                                <input type="email" name="email" id="email" class="form-control" value="{{ $provider->email }}" placeholder="" required>
                            </div>

                            <div class="form-group col-6">
                                <label for="addres">Dirección :</label>
                                <input type="text" name="addres" id="addres" class="form-control" value="{{ $provider->addres }}" placeholder="">
                            </div>

                            <div class="form-group col-6">
                                <label for="photo">Telefono / Celular :</label>
                                <input type="text" name="photo" id="photo" class="form-control" value="{{ $provider->photo }}" placeholder="">
                            </div> --}}
                            
                            <div class="text-center card-footer">
                                <button type="submit" class="btn btn-success mr-2">Actualizar</button>
                                <a href="{{ route('providers.index') }}" class="btn btn-info">Cancelar</a>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    
@endsection