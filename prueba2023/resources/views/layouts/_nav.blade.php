<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item nav-profile">
        <div class="nav-link">
          <div class="profile-image">
            <img src="{{asset('image/perfil.png')}}" alt="image" style="width:65px"/>
          </div>
          <div class="profile-name">
            <p class="name">
              {{ Auth::user()->name }}
            </p>
            <p class="designation">
              {{ Auth::user()->email }}
            </p>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="fa fa-home menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#page-layouts3" aria-expanded="false" aria-controls="page-layouts3">
          <i class="fa menu-icon">&#xf013;</i>
          <span class="menu-title">Configuración</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="page-layouts3">
          <ul class="nav flex-column sub-menu">
            @can('category_index')
              <li class="nav-item">
                <a class="nav-link" href="{{ route('platos.index') }}">
                  <span class="menu-title">Platos</span>
                </a>
              </li>
            @endcan
          </ul>
        </div>
      </li>
    </ul>
  </nav>